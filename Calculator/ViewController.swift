//
//  ViewController.swift
//  Calculator
//
//  Created by Ola Sundbo Halvorsen on 31/10/2015.
//  Copyright © 2015 olahalvorsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var newInput:Bool = true
    var workingValue:Float = 0
    var result:Float = 0
    var currentOperator:operators = .add
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func numberButton(sender: UIButton) {
        if(newInput) {
            resultLabel.text! = "";
            newInput = false;
        }
        
        resultLabel.text = resultLabel.text! + sender.titleLabel!.text!
        workingValue = Float(resultLabel.text!)!
    }
    
    @IBAction func operatorButton(sender: UIButton) {
        
        switch(sender.titleLabel!.text!) {
            case "+":
                currentOperator = .add
            case "-":
                currentOperator = .subtract
            case "*":
                currentOperator = .multiply
            default:
                break;
        }
        
        result = calculate(result, numberTwo: workingValue)
        resultLabel.text = String(result)
        newInput = true
        workingValue = 0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calculate(numberOne:Float, numberTwo:Float) -> Float {
        switch currentOperator {
        case .add:
            return numberOne + numberTwo
        case .subtract:
            return numberOne - numberTwo
        case .multiply:
            return numberOne * numberTwo
        }
    }
    
    enum operators {
        case add, subtract, multiply
    }
}

